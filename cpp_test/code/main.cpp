//
//  main.cpp
//  DockerApp
//
//  Created by Pablo Caballero on 04/03/2020.
//  Copyright © 2020 Pablo Caballero. All rights reserved.
//

#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, const char * argv[]) {
    if (argc != 2) {
        cout << "You need to specify the file\n";
        exit(1);
    }
    
    string line;
    ifstream input(argv[1]);
    
    if (input.is_open()) {
        while (getline(input,line))
          cout << line << '\n';
        
        input.close();
        
        ofstream outfile ("files/result.txt");
        outfile << "Prueba finalizada" << endl;
        outfile.close();
    } else
        cout << "The file you specified is not valid\n";
    
    return 0;
}
